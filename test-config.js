exports.config = {
    runner: 'local',
    port: 4723,
    host: 'localhost',
    path: '/wd/hub',
    logLevel: 'info',
    framework: 'mocha',
    mochaOpts:{
        ui: 'bdd',
        require: ['@babel/register'],
        timeout: 600000
    },
    maxInstances: 1,
    sync: true,
    specs:[
        './src/test_scripts/tc_001_Login.js',
        './src/test_scripts/tc_002_BeautyShop.js'
    ],
    capabilities:[{
        "platformName": "Android",
        "automationName": "UiAutomator2",
        "udid": "emulator-5554",
        "appPackage": "com.airasia.mobile.debug",
        "appActivity": "com.airasia.mobile.MainActivity2"
    }]

}