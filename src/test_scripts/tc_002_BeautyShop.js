import GoToLoginPage from '../page_objects/GoToLoginPage'
import LoginPage from '../page_objects/LoginPage'
import BeautyShopPage from '../page_objects/BeautyShopPage'

describe('Validate Payment Section',() => {

    before('should go to login page', () => {
        
        GoToLoginPage.go_to_login();
        LoginPage.login_to_shop();
        browser.pause(10000)

    })

    it('Add Item to cart', () => {
        BeautyShopPage.add_to_cart();
    })
    it('item must be added to cart', ()=>{
        expect(BeautyShopPage.iFan_item).toBeDisplayed()
        console.log(expect(BeautyShopPage.iFan_item).toBeDisplayed())
    })
    it('Select Delivery Time', ()=> {
        BeautyShopPage.select_time_delivery()
        browser.pause(5000)
    })
    it('delivery time must be selected, edit button shown', ()=>{
        expect(BeautyShopPage.edit_button).toBeDisplayed()
        console.log(expect(BeautyShopPage.edit_button).toBeDisplayed())
    })
    it('Continue to Payment', ()=> {
        BeautyShopPage.continue_paybutton.click()
        browser.pause(15000)
    })
    it('secure checkout must be shown', ()=> {
        expect(BeautyShopPage.secure_checkout).toBeDisplayed()
        console.log(expect(BeautyShopPage.secure_checkout).toBeDisplayed())
    })

})