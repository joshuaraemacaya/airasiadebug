import GoToLoginPage from '../page_objects/GoToLoginPage'
import LoginPage from '../page_objects/LoginPage'

describe('Confirm Login',() => {

    before('should go to login page', () => {
        GoToLoginPage.go_to_login();
    })

    it('login page must be displayed', () => {
        expect(GoToLoginPage.email_textfield).toBeDisplayed()
        console.log(expect(GoToLoginPage.email_textfield).toBeDisplayed())
    })

    it('Login to shop', ()=> {
        LoginPage.login_to_shop();
        browser.pause(10000)
    })
    it('cart item must be displayed after login', ()=> {
        expect(LoginPage.cart_button).toBeDisplayed()
        console.log(expect(LoginPage.cart_button).toBeDisplayed())
    })

})