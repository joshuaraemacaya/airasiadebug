const iFanItemSelector = 'new UiSelector().resourceId("Beauty_Widget-carousel-carouselCard-1")'
const iFanItem = `android=${iFanItemSelector}`
const add2CartSelector ='new UiSelector().text("Add to cart")'
const add2Cart = `android=${add2CartSelector}`
const cartButtonSelector = 'new UiSelector().className("android.widget.Image").instance(2).index(0)'
const cartButton = `android=${cartButtonSelector}`
const selectSelector = 'new UiSelector().text("Select")'
const selectButton = `android=${selectSelector}`
const confirmDeliveryTime = '~Confirm'
const continue2PayButton = '~this is an a11y custom label'
const secureCheckoutSelector = 'new UiSelector().text("Secure Checkout")'
const secureCheckout = `android=${secureCheckoutSelector}`
const editSelector = 'new UiSelector().className("android.widget.Button").text("Edit").index(3)'
const editButton = `android=${editSelector}`
const iFanAddedSelector = 'new UiSelector().text("iFan")'
const iFanAdded = `android=${iFanAddedSelector}`

class BeautyShopPage{

    add_to_cart(){

        $(iFanItem).waitForDisplayed({timeout: 20000})
        $(iFanItem).click();
        browser.pause(10000)
        $(add2Cart).waitForDisplayed({timeout: 5000})
        $(add2Cart).click()
        browser.pause(10000)
        $(cartButton).waitForDisplayed({timeout: 5000})
        $(cartButton).click()
        browser.pause(10000)
    }

    select_time_delivery(){
        
        $(selectButton).waitForDisplayed({timeout: 5000})
        $(selectButton).click()
        $(confirmDeliveryTime).waitForDisplayed({timeout: 50000})
        $(confirmDeliveryTime).click()

    }
    get iFan_item(){
        $(iFanAdded).waitForDisplayed({timeout: 30000})
        return $(iFanAdded)
    }
    get continue_paybutton(){
        $(continue2PayButton).waitForDisplayed({timeout: 30000})
        return $(continue2PayButton)
    }
    get edit_button(){
        $(editButton).waitForDisplayed({timeout: 30000})
        return $(editButton)
    }
    get secure_checkout(){
        $(secureCheckout).waitForDisplayed({timeout: 30000})
        return $(secureCheckout)
    }



}
export default new BeautyShopPage()