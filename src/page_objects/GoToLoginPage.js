const maybeLater = 'new UiSelector().resourceId("com.airasia.mobile.debug:id/button2")'
const maybeLaterButton = `android=${maybeLater}`
const continueHomePage = 'new UiSelector().resourceId("com.airasia.mobile.debug:id/tooltip_dialog_content_view")'
const continueHomePageButton = `android=${continueHomePage}`
const beautyIcon = 'new UiSelector().className("android.view.ViewGroup").instance(0).index(2)'
const beautyIconButton = `android=${beautyIcon}`
const cartIconButton = '~0'
const loginEmailSelector = 'new UiSelector().resourceId("text-input--login")'
const loginEmail = `android=${loginEmailSelector}`

class GoToLoginPage{

    go_to_login(){
        $(maybeLaterButton).waitForDisplayed({timeout: 30000})
        $(maybeLaterButton).click();
        $(continueHomePageButton).click();
        $(beautyIconButton).waitForDisplayed({timeout: 60000})
        $(beautyIconButton).click();
        $(cartIconButton).waitForDisplayed({timeout: 60000})
        $(cartIconButton).click();
    }
    get email_textfield(){
        $(loginEmail).waitForDisplayed({timeout: 30000})
        return $(loginEmail)
    }
    
}

export default new GoToLoginPage()