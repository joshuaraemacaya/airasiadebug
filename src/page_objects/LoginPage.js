// import $ from "webdriverio/build/commands/browser/$"

const loginEmailSelector = 'new UiSelector().resourceId("text-input--login")'
const loginEmail = `android=${loginEmailSelector}`
const loginPasswordSelector = 'new UiSelector().resourceId("password-input--login")'
const loginPassword = `android=${loginPasswordSelector}`
const loginButtonSelector = 'new UiSelector().resourceId("loginbutton")'
const loginButton = `android=${loginButtonSelector}`
const iFanItemSelector = 'new UiSelector().resourceId("Beauty_Widget-carousel-carouselCard-1")'
const iFanItem = `android=${iFanItemSelector}`

class LoginPage{

    login_to_shop(){
        $(loginEmail).waitForDisplayed({timeout: 20000})
        $(loginEmail).setValue('bigset16acc.1@gmail.com');
        $(loginPassword).waitForDisplayed({timeout: 20000})
        $(loginPassword).setValue('P@55w0rd!23');
        $(loginButton).waitForDisplayed({timeout: 20000});
        $(loginButton).click();
    }
    get cart_button(){
        $(iFanItem).waitForDisplayed({timeout: 20000})
        return $(iFanItem)
    }


}
export default new LoginPage()